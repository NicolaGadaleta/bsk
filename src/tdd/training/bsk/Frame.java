package tdd.training.bsk;

public class Frame {
	private int firstThrow = 0;
	private int secondThorw = 0;
	private int bonus = 0;
	private int multiplier = 1;
	
	public static final int MIN_THROW_VALUE = 0;
	public static final int MAX_THROW_VALUE = 10;
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		
		if(firstThrow < MIN_THROW_VALUE || secondThrow < MIN_THROW_VALUE) {
			throw new BowlingException("Negative Throw value");
		}else if(firstThrow > MAX_THROW_VALUE || secondThrow > MAX_THROW_VALUE){
			throw new BowlingException("Invalid Throw value");
		}else if(firstThrow + secondThrow > MAX_THROW_VALUE) {
			throw new BowlingException("The Throw's sum should be ten or less");
		}else {
			this.firstThrow = firstThrow;
			this.secondThorw = secondThrow;
		}
		
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return this.firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		
		return this.secondThorw;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return this.bonus;
	}
	
	/**
	 * It sets the frame multiplier
	 * 
	 * @param multiplier
	 */
	public void setMultiplier(int multiplier) {
		this.multiplier = multiplier;
	}
	
	/**
	 * It return the multiplier of this frame
	 * 
	 * @return the multiplier
	 */
	public int getMultiplier() {
		return this.multiplier;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		return ((this.firstThrow + this.secondThorw)* this.multiplier) + this.bonus;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return ((firstThrow + secondThorw) == MAX_THROW_VALUE) && (firstThrow == MAX_THROW_VALUE);
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return ((firstThrow + secondThorw) == MAX_THROW_VALUE) && (firstThrow != MAX_THROW_VALUE);
	}

}
