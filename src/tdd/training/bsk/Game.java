package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	private ArrayList<Frame> frames = null;
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;
	private static final int MIN_FRAMES = 0;
	private static final int STRIKE_COUNTER_TRIGGER = 3;
	private static final int MAX_FRAMES = 10;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		if(frames.size() >= MAX_FRAMES) {
			throw new BowlingException("Too many frames");
		}else {
			frames.add(frame);
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index > MAX_FRAMES-1 || index < MIN_FRAMES) {
			throw new BowlingException("Invalid position");
		}
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(firstBonusThrow < Frame.MIN_THROW_VALUE || firstBonusThrow > Frame.MAX_THROW_VALUE) {
			throw new BowlingException("Bonus Throw invalid value");
		}
		
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(secondBonusThrow < Frame.MIN_THROW_VALUE || secondBonusThrow > Frame.MAX_THROW_VALUE) {
			throw new BowlingException("Bonus Throw invalid value");
		}
		
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}
	
	
	/**
	 * It return the number of consecutive strikes
	 * 
	 * @param position: starting position
	 * @return The number of consecutive strikes
	 */
	public int getConsecutiveStrikes(int position) {
		int consecutiveStrikes = 0;
		int index = 0;
		
		for(index = position;index <= position+2 && index < frames.size();index ++) {
			if(frames.get(index).isStrike()) {
				consecutiveStrikes++;
			}else {
				break;
			}
		}
		return consecutiveStrikes;
	}
	
	/**
	 * It return the last value to add to the game score in case of strike or spare ending
	 * 
	 * @return the value to add to the game score
	 */
	public int getAddValue() {
		int addValue = 0;
		
		if(frames.get(MAX_FRAMES - 1).isSpare()) {
			addValue = this.firstBonusThrow;
			
		}else if(frames.get(MAX_FRAMES - 1).isStrike()) {
			
			if(this.firstBonusThrow == Frame.MAX_THROW_VALUE && this.secondBonusThrow == Frame.MAX_THROW_VALUE) {
				addValue = frames.get(MAX_FRAMES - 1).getScore() + this.firstBonusThrow + this.secondBonusThrow;
			}else {
				addValue = this.firstBonusThrow + this.secondBonusThrow;
			}
		}
		return addValue;
	}
	
	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int gameScore = 0;
		int position = 0;
		int nextFramePosition = 0;
		int bonus = 0;
		int strikeMultiplier = 0;
		Frame nextFrame = null;
		
		if(frames == null) {
			throw new BowlingException("There are no frames");
		}
		
		for(Frame frame:frames) {
			if(position != MAX_FRAMES-1) {
				
				if(frame.isSpare()) {
					nextFramePosition = position + 1;
					nextFrame = frames.get(nextFramePosition);
					bonus = nextFrame.getFirstThrow();
					
				}else if(frame.isStrike()) {
					strikeMultiplier = getConsecutiveStrikes(position);
					
					if(strikeMultiplier == STRIKE_COUNTER_TRIGGER) {
						frame.setMultiplier(strikeMultiplier);
						
					}else if(strikeMultiplier > 1 && (position + strikeMultiplier) < MAX_FRAMES) {
						frame.setMultiplier(strikeMultiplier);
						
						nextFramePosition = position + strikeMultiplier;
						nextFrame = frames.get(nextFramePosition);
						bonus = nextFrame.getFirstThrow();
											
					}else{
						nextFramePosition = position + 1;
						nextFrame = frames.get(nextFramePosition);
						bonus = nextFrame.getFirstThrow() + nextFrame.getSecondThrow();	
					}		
				}
				frame.setBonus(bonus);
				nextFrame = null;
			}	
			bonus = 0;
			gameScore += frame.getScore();
			position ++;
		}
		
		gameScore+= getAddValue();
		return gameScore;	
	}
	
}
