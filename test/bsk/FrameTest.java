package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test(expected = BowlingException.class)
	public void frameCreationShouldReturnAnExceptionOnFirstThrow() throws BowlingException {
		Frame frame = new Frame(-1,0);
	}
	
	@Test(expected = BowlingException.class)
	public void frameCreationShouldReturnAnExceptionOnSecondThrow() throws BowlingException {
		Frame frame = new Frame(0,-1);
	}
	
	@Test(expected = BowlingException.class)
	public void frameCreationShouldReturnAnException() throws BowlingException {
		Frame frame = new Frame(11,0);
	}
	
	@Test(expected = BowlingException.class)
	public void frameCreationShouldReturnAnExceptionOnThrows() throws BowlingException {
		Frame frame = new Frame(10,1);
	}
	
	@Test
	public void getFirstThrowShouldReturnTwo() throws BowlingException{
		int firstThrow = 0;
		Frame frame = new Frame(2,4);
		
		firstThrow = frame.getFirstThrow();
		
		assertEquals(firstThrow,2);
	}
	
	@Test
	public void getSecondThrowShouldReturnFour() throws BowlingException{
		int secondThrow = 0;
		Frame frame = new Frame(2,4);
		
		secondThrow = frame.getSecondThrow();
		
		assertEquals(secondThrow,4);
	}
	
	@Test
	public void getScoreShouldReturnEight() throws BowlingException {
		int score = 0;
		Frame frame = new Frame(2,6);
		
		score = frame.getScore();
		
		assertEquals(score,8);
	}
	
	@Test
	public void getBonusShouldReturnTwo() throws BowlingException{
		int bonus = 0;
		Frame frame = new Frame(2,6);
		
		frame.setBonus(2);
		bonus = frame.getBonus();
		
		assertEquals(bonus,2);
		
	}
	
	@Test
	public void isSpareShouldReturnTrue() throws BowlingException{
		boolean isSpare = false;
		Frame frame = new Frame(1,9);
		
		isSpare = frame.isSpare();
		
		assertTrue(isSpare);
	}
	
	@Test
	public void isSpareShouldReturnFalse() throws BowlingException{
		boolean isSpare = false;
		Frame frame = new Frame(2,6);
		
		isSpare = frame.isSpare();
		
		assertFalse(isSpare);
	}
	
	@Test
	public void getScoreShouldReturnThirteen() throws BowlingException{
		int bonus = 3;
		int frameScore = 0;
		Frame frame = new Frame(1,9);
		
		frame.setBonus(bonus);
		frameScore = frame.getScore();
		
		assertEquals(frameScore,13);
	}
	
	@Test
	public void isStrikeShouldReturnTrue() throws BowlingException{
		boolean isStrike = false;
		Frame frame = new Frame(10,0);
		
		isStrike = frame.isStrike();
		
		assertTrue(isStrike);
	}
	
	@Test
	public void getScoreShouldReturnNinetheen() throws BowlingException{
		int frameScore = 0;
		Frame frame = new Frame(10,0);
		Frame nextFrame = new Frame(3,6);
		
		frame.setBonus(nextFrame.getFirstThrow() + nextFrame.getSecondThrow());
		frameScore = frame.getScore();
		
		assertEquals(frameScore,19);
	}
	
	@Test
	public void getMultilpierShouldReturnTwo() throws BowlingException{
		int frameMultiplier = 1;
		Frame frame = new Frame(10,0);
		
		frame.setMultiplier(2);
		frameMultiplier = frame.getMultiplier();
		
		assertEquals(frameMultiplier,2);
	}
	
}
