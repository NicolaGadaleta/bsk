package bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	private Game game = null;
	
	@Before
	public void SetUp() {
		game = new Game();
	}
	
	
	
	@Test(expected = BowlingException.class)
	public void addFrameShouldReturnAnException() throws BowlingException {
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		game.addFrame(new Frame(10,0));
		
		
	}
	
	
	@Test
	public void getFrameAtZeroPositionShouldReturnOneFive() throws BowlingException {
		Frame zeroPosition = null;
		int firstThorw = 0;
		int secondThrow = 0;
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		zeroPosition = game.getFrameAt(0);
		
		firstThorw = zeroPosition.getFirstThrow();
		secondThrow = zeroPosition.getSecondThrow();
		
		assertArrayEquals(new int[] {firstThorw,secondThrow},new int[] {1,5});
		
	}
	
	@Test(expected = BowlingException.class)
	public void getFrameAtTenthPositionShouldReturnAnException() throws BowlingException {
		Frame positionFrame = null;
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		positionFrame = game.getFrameAt(10);
		
		
	}
	
	@Test(expected = BowlingException.class)
	public void getFrameAtNegativePositionShouldReturnAnException() throws BowlingException {
		Frame positionFrame = null;

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		positionFrame = game.getFrameAt(-1);
		
		
	}
	
	@Test
	public void calculateScoreShouldReturnEightyOne() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,81);
	}
	
	@Test
	public void calculateScoreShouldReturnEightyEight() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,88);
	}
	
	@Test
	public void calculateScoreShouldReturnNinetyFour() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,94);
	}
	
	@Test
	public void calculateScoreShouldReturnEightyFive() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(2,6));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,85);
	}
	
	@Test
	public void calculateScoreShouldReturnOneHundredThree() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,103);
	}
	
	@Test
	public void getConsecutiveStrikesShouldReturnTwo() throws BowlingException {
		int consecutiveStrike = 0;
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		consecutiveStrike = game.getConsecutiveStrikes(0);
		
		assertEquals(consecutiveStrike,2);
	}
	
	@Test
	public void calculateScoreShouldReturnOneHundredTwelve() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,112);
	}
	
	@Test
	public void calculateScoreShouldReturnNinetyEight() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,98);
	}
	
	@Test
	public void getFirstBonusThrowShouldBeSeven()throws BowlingException {
		int firstBonusThrow = 0;
		game.setFirstBonusThrow(7);
		
		firstBonusThrow = game.getFirstBonusThrow();
		
		assertEquals(firstBonusThrow,7);
	}
	
	@Test(expected = BowlingException.class)
	public void setFirstBonusThrowShouldReturnAnExceptionNegativeValue()throws BowlingException {
		game.setFirstBonusThrow(-1);
	}
	
	@Test(expected = BowlingException.class)
	public void setFirstBonusThrowShouldReturnAnExceptionOverTheMaxValue()throws BowlingException {
		game.setFirstBonusThrow(11);
	}
	
	@Test
	public void calculateScoreShouldReturnNinety() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		
		game.setFirstBonusThrow(7);
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,90);
	}
	
	@Test
	public void getSecondBonusThrowShouldBeSeven()throws BowlingException {
		int secondBonusThrow = 0;
		game.setSecondBonusThrow(7);
		
		secondBonusThrow = game.getSecondBonusThrow();
		
		assertEquals(secondBonusThrow,7);
	}
	
	@Test(expected = BowlingException.class)
	public void setSecondBonusThrowShouldReturnAnExceptionNegativeValue()throws BowlingException {
		game.setSecondBonusThrow(-1);
	}
	
	@Test(expected = BowlingException.class)
	public void setSecondBonusThrowShouldReturnAnExceptionOverTheMaxValue()throws BowlingException {
		game.setSecondBonusThrow(11);
	}
	
	@Test
	public void calculateScoreShouldReturnNinetyTwo() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,92);
	}
	
	@Test
	public void calculateScoreShouldReturnBestScore() throws BowlingException {
		int gameScore = 0;
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		gameScore = game.calculateScore();
		
		assertEquals(gameScore,300);
	}
}
